#!/bin/sh

set -e

TDIR=$1
MIRROR=$2
DISKNUM=$3
CDDIR=$4
ARCHES=$5

cd $CDDIR

fix_branding() {
    sed -i -e 's|Debian GNU/Linux|Parrot OS|g' \
	-e 's|Parrot GNU/Linux|Parrot OS|g' \
	-e 's|Debian parrot|Parrot OS|g' \
	-e 's|Debian|parrot|g' \
	-e 's|DEBIAN|PARROT|g' \
	"$@"
}

# Rebrand Debian into Parrot
if [ -e ../boot$DISKNUM/isolinux/menu.cfg ]; then
    fix_branding ../boot$DISKNUM/isolinux/menu.cfg \
		 ../boot$DISKNUM/isolinux/*.txt
fi
if [ -d boot/grub/theme ] && [ -s boot/grub/theme ]; then
    fix_branding boot/grub/theme/*
fi
if [ -e autorun.inf ]; then
    fix_branding autorun.inf
fi

# Replace Debian specific documentation
rm -rf css
cat >README.txt <<EOF
This disc contains an installer for Parrot OS.

Read more at: https://www.parrotsec.org
EOF
cat >README.html <<EOF
<html>
<head><title>Parrot OS Installer Disc</title></head>
<body>
This disc contains an installer for Parrot OS.

Read more at: <a href="https://www.parrotsec.org">www.parrotsec.org</a>
</body>
</html>
EOF

# Fix local package mirror
sed -i 's/stable/lory/g' dists/lory/main/binary-amd64/Release || true
sed -i 's/stable/lory/g' dists/lory/main/binary-i386/Release || true
sed -i 's/stable/lory/g' dists/lory/main/binary-arm64/Release || true
sed -i 's/stable/lory/g' dists/lory/main/binary-armhf/Release || true
sed -i 's/stable/lory/g' dists/lory/contrib/binary-amd64/Release || true
sed -i 's/stable/lory/g' dists/lory/contrib/binary-i386/Release || true
sed -i 's/stable/lory/g' dists/lory/contrib/binary-arm64/Release || true
sed -i 's/stable/lory/g' dists/lory/contrib/binary-armhf/Release || true
sed -i 's/stable/lory/g' dists/lory/non-free/binary-amd64/Release || true
sed -i 's/stable/lory/g' dists/lory/non-free/binary-i386/Release || true
sed -i 's/stable/lory/g' dists/lory/non-free/binary-arm64/Release || true
sed -i 's/stable/lory/g' dists/lory/non-free/binary-armhf/Release || true


# Redo the md5sum.txt due to our changes
find . -type f | grep -v ./md5sum.txt | xargs md5sum | sort -uk2 > md5sum.txt
